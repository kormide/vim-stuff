" Call pathogen
call pathogen#infect()
call pathogen#helptags()

" Set default colourscheme
colorscheme molokai

" Enable filetype stuff
filetype on
filetype plugin on
filetype indent on

" VimOrganizer settings
filetype plugin indent on
au! BufRead,BufWrite,BufWritePost,BufNewFile *.org 
au BufEnter *.org            call org#SetOrgFileType()

" Turn on syntax highlighting
syntax on

" Don't wrap text
set nowrap

" Allow hidden buffers
set hidden

" Show line numbers
set number

" Don't upate display while executing macros
set lazyredraw

" Display what mode you're in
set showmode

" tell VIM to always put a status line in, even if there is only one window
set laststatus=2

" Enable enhanced command-line completion
set wildmenu

" Use spaces instead of tabs
set expandtab

" Use 4 space widths for tabs
set tabstop=4
set shiftwidth=4

autocmd BufRead,BufNewFile  *.html,*.css set expandtab tabstop=2 shiftwidth=2

" Make command line two lines high
set ch=2

" Searching
set ignorecase
set smartcase
set incsearch
set hlsearch

" Automatically read a file that has changed on disk
set autoread

" OK, so I'm gonna remove the VIM safety net for a while and see if kicks my ass
set nobackup
set nowritebackup
set noswapfile

" Set up the gui cursor to look nice
set guicursor=n-v-c:block-Cursor-blinkon0,ve:ver35-Cursor,o:hor50-Cursor,i-ci:ver25-Cursor,r-cr:hor20-Cursor,sm:block-Cursor-blinkwait175-blinkoff150-blinkon175

" Turn off that stupid highlight search
nmap <silent> ,n :nohls<CR>

" Auto-reload files when changed
set autoread

" NERDTree mapping
nmap <silent> <F7> :NERDTreeToggle<CR>

" Maps to make handling windows a bit easier
noremap <silent> ,h :wincmd h<CR>
noremap <silent> ,j :wincmd j<CR>
noremap <silent> ,k :wincmd k<CR>
noremap <silent> ,l :wincmd l<CR>
noremap <silent> ,sb :wincmd p<CR>
noremap <silent> <C-F9>  :vertical resize -10<CR>
noremap <silent> <C-F10> :resize +10<CR>
noremap <silent> <C-F11> :resize -10<CR>
noremap <silent> <C-F12> :vertical resize +10<CR>
noremap <silent> ,s8 :vertical resize 83<CR>
noremap <silent> ,cj :wincmd j<CR>:close<CR>
noremap <silent> ,ck :wincmd k<CR>:close<CR>
noremap <silent> ,ch :wincmd h<CR>:close<CR>
noremap <silent> ,cl :wincmd l<CR>:close<CR>
noremap <silent> ,cc :close<CR>
noremap <silent> ,cw :cclose<CR>
noremap <silent> ,ml <C-W>L
noremap <silent> ,mk <C-W>K
noremap <silent> ,mh <C-W>H
noremap <silent> ,mj <C-W>J
noremap <silent> <C-7> <C-W>>
noremap <silent> <C-8> <C-W>+
noremap <silent> <C-9> <C-W>+
noremap <silent> <C-0> <C-W>>

" Show the bookmarks table on startup
let NERDTreeShowBookmarks=1

" Don't display these kinds of files
let NERDTreeIgnore=[ '\.ncb$', '\.suo$', '\.vcproj\.RIMNET', '\.obj$',
                   \ '\.ilk$', '^BuildLog.htm$', '\.pdb$', '\.idb$',
                   \ '\.embed\.manifest$', '\.embed\.manifest.res$',
                   \ '\.intermediate\.manifest$', '^mt.dep$' ]
